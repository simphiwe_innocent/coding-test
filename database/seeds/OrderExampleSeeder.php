<?php

use Illuminate\Database\Seeder;

class OrderExampleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $new_order = new \App\Order();
        $new_order->user_id = \App\User::all()->first()->id;
        $new_order->save();

        $cart_item = new \App\OrderItem([
            'product_id' => 1,
            'variant_id' => 1,
            'qty' => 3
        ]);

        $new_order->orderItems()->save($cart_item);
    }
}
