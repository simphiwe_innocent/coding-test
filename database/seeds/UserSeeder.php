<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
                [
                    'name'              => 'joe',
                    'email'             => 'joe@shop.test',
                    'email_verified_at' => now(),
                    'password'          => bcrypt('test12'),
                    'remember_token'    => Str::random(10),
                ],
                [
                    'name'              => 'jill',
                    'email'             => 'jill@shop.test',
                    'email_verified_at' => now(),
                    'password'          => bcrypt('test34'),
                    'remember_token'    => Str::random(10),
                ],
                [
                    'name'              => 'Jules',
                    'email'             => 'jules@arca.test',
                    'email_verified_at' => now(),
                    'password'          => bcrypt('test56'),
                    'remember_token'    => Str::random(10),
                ],
            ]
        );
    }
}
