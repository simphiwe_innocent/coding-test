<?php

use App\Product;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /**
         * Create Products
         */
        DB::table('products')->insert([
           [
               'name' => 'T-shirt',
               'description' => 'Plain t-shirt'
           ],
            [
                'name' => 'Pants',
                'description' => 'Long Pants'
            ],
            [
                'name' => 'Socks',
                'description' => 'Super Socks'
            ],
            [
                'name' => 'Wallet',
                'description' => 'Generic Wallet'
            ],
            [
                'name' => 'Shoes',
                'description' => 'Street Wise Shoes'
            ],
        ]);

        /**
         * Create Variants
         */
        DB::table('variants')->insert([
            [
              'name' => 'black',
              'value' => 'black',
            ],
            [
              'name' => 'white',
              'value' => 'white',
            ],
            [
              'name' => 'Red',
              'value' => 'red',
            ],
            [
              'name' => 'Purple',
              'value' => 'purple',
            ],
            [
              'name' => 'Jean Fabric',
              'value' => 'jean',
            ],
            [
              'name' => 'Leather Fabric',
              'value' => 'leather',
            ],
            [
              'name' => 'Polka Dot',
              'value' => 'polka',
            ],
            [
              'name' => 'Classic',
              'value' => 'classic',
            ],
        ]);

        /**
         * Assign Variants to Products
         */

        $product1 = Product::find(1)->variants()->saveMany(\App\Variant::whereIn('id',[1,2,3,4])->get());
        $product2 = Product::find(2)->variants()->saveMany(\App\Variant::whereIn('id',[5,6])->get());
        $product3 = Product::find(3)->variants()->saveMany(\App\Variant::all());
        $product4 = Product::find(4)->variants()->saveMany(\App\Variant::whereIn('id',[1,3,4,5,7])->get());
        $product5 = Product::find(5)->variants()->saveMany(\App\Variant::whereIn('id',[1,7])->get());



    }
}
