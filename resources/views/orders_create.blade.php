<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Font Awesome Icons -->
    <link rel="stylesheet" href="{{ asset("/bower_components/admin-lte/plugins/fontawesome-free/css/all.min.css") }}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset("/bower_components/admin-lte/dist/css/adminlte.min.css") }}">
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

    <!-- DataTables -->
    <link rel="stylesheet" href="{{ asset("/bower_components/admin-lte/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css")}}">
    <link rel="stylesheet" href="{{ asset("/bower_components/admin-lte/plugins/datatables-responsive/css/responsive.bootstrap4.min.css")}}">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.6.4/css/buttons.dataTables.min.css"/>

</head>

<body class="hold-transition sidebar-mini">
<div class="wrapper">


    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">Create new order</h1>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-6">
                        <div class="card card-primary" >
                            <div class="card-header">
                                <h3 class="card-title">New order</h3>
                            </div>
                            <div class="card-body">
                                <a href="{{route('orders.index')}}">Back</a>

                                <form method="POST" action="{{route('orders.store')}}" >
                                    @method('POST')
                                    @csrf
                                    <div class="form-group">
                                        <label>Select User: </label>
                                        <select name="user_id" class="custom-select">
                                            @foreach($users as $user)
                                                <option value="{{$user->id}}">{{$user->name}} - {{$user->email}}</option>
                                            @endforeach
                                        </select>
                                    </div>


                                        <table class="table ">
                                            <tr>
                                                <td><label>Product Name</label></td>
                                                <td><label>Product Description</label></td>
                                            </tr>
                                            @foreach($products as $product)
                                                <tr>
                                                    <td><h3>{{$product->name}}</h3></td>
                                                    <td><h3>{{$product->description}}</h3></td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        @foreach($product->variants as $variant)
                                                            <tr>
                                                                <td>{{$variant->name}}</td>
                                                                <td><input type="number" min="0" style="width: 30px;" value="0" id="qty[{{$product->id}}][{{$variant->id}}]" name="qty[{{$product->id}}][{{$variant->id}}]"></td>
                                                            </tr>
                                                        @endforeach
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><button id="qty[{{$product->id}}][{{$variant->id}}]" onclick="placeOrder(this.id)" class="btn btn-block btn-primary" type="submit">Order - {{ $product->name }}</button></td>
                                                </tr>
                                                @endforeach

                                        </table>

                                </form>

                            </div>
                        </div>
                    </div>
                    <!-- /.col-md-12 -->
                </div>
                <!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
</div>
<!-- ./wrapper -->

<script>
    function placeOrder(id) {
        var productIdQTY = document.getElementById(id).value;

        alert(productIdQTY);
    }
</script>


