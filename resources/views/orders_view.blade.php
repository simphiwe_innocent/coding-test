@extends('layouts.app')

@section('content')
    <div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">View Order</h3>

                        <div class="float-right">
                            <a href="{{ url('products') }}">Add Product</a>
                        </div>

                    </div>
                    <div class="card-body">

                        @if (\Session::has('success'))
                            <div class="alert alert-success">
                                    {!! \Session::get('success') !!}
                            </div>
                        @endif

                        <a href="{{route('orders.index')}}">Back</a>
                        <br>
                        <br>
                        <h3>Order Details</h3>
                        <label>User Name : </label> {{$order->user->name}}<br>
                        <label>User Email : </label> {{$order->user->email}}<br>
                        <label>Order ID : </label> {{$order->id}}<br>
                        <label>Order Created at : </label> {{$order->created_at}}<br><br><br>


                        <h3>Order Items</h3>
                        <table id="example2" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <td>Product</td>
                                <td>Variant</td>
                                <td>Quantity</td>
                                <td>&nbsp;</td>
                            </tr>
                            </thead>
                            @foreach($order->OrderItems as $orderItem)
                                <tr>
                                    <td>{{$orderItem->product->name}}</td>
                                    <td>{{$orderItem->variant->name}}</td>
                                    <td>{{$orderItem->qty}}</td>
                                    <td>
                                        <a href="{{ url('delete-product/' . $orderItem->id) }}" onclick="return confirm('Are you sure you want to delete this item?')"; class="btn btn-sm btn-danger">
                                            <i class="fa fa-trash"></i></a></td>
                                </tr>
                            @endforeach
                        </table>

                        <a class="btn btn-sm btn-success" href="{{ url('export-product/' . $order->id) }}">Export PDF</a>
                    </div>
                </div>
            </div>
            <!-- /.col-md-12 -->
        </div>
        <!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content -->
@endsection