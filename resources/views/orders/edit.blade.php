@extends('layouts.app')

@section('content')
    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6">
                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title">Assign User</h3>
                        </div>
                        <div class="card-body">

                            <a href="{{route('orders.index')}}">Back</a>

                            <form method="POST" action="{{route('orders.update',$order->id)}}">
                                @method('PUT')
                                @csrf
                                <label>User:</label>
                                <select name="user_id" class="custom-select">
                                    @foreach($users as $user)
                                        <option
                                                @if($user->id == $order->user_id)
                                                selected
                                                @endif
                                                value="{{$user->id}}">{{$user->name}} - {{$user->email}}

                                        </option>
                                    @endforeach
                                </select>
                                <div>&nbsp;</div>
                                <button class="btn btn-primary" type="submit">Assign User</button>
                            </form>
                        </div>
                    </div>
                </div>
                <!-- /.col-md-12 -->
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
@endsection