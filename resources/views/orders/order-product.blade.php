@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Order Product</div>

                    <div class="card-body">

                        <form method="POST" action="{{route('orders.store')}}">
                            @csrf
                        {{ $product->name }} - {{ $product->description }}



                            <div class="form-group">
                                <label for="qty">Qauntity</label>
                                <input type="number" name="qty" required class="form-control" placeholder="Enter quantity">
                            </div>

                            <select class="form-control" id="variant" name="variant">
                                @foreach($variant as $variant)
                                    <option value="{{ $variant->id }}">{{ $variant->name }}</option>
                                @endforeach
                            </select>

                            <input name="productId" value="{{ $product->id }}" type="hidden">

                            <button type="submit" class="btn btn-success my-1">
                                <i class="fa fa-save"></i> &nbsp; Save</button>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
