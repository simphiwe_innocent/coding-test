<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('dashboard', 'DashboardController@index');

Route::get('products', 'ProductsController@index')->middleware('auth');
Route::get('delete-product/{orderItem}', 'ProductsController@deleteItem')->middleware('auth');

Route::resource('orders','OrderController')->middleware('auth');
Route::get('order-product/{product}','OrderController@orderProduct')->middleware('auth');
Route::get('export-product/{order}','OrderController@exportPDF')->middleware('auth');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home')->middleware('auth');
