## Instructions
A static Order System is in need of an overhaul and needs to be introduced to the new century. Use any javascript framework (except jquery) and css framework to revamp the front end.
Three new features are also required:
- User authentication
- Order Search Functionality
- Order PDF Download Functionality

### Checklist
- Javascript framework used. e.g. Vue
- CSS framework used. e.g. Bootstrap
- User Login
- User Registration
- Order Search
- Pdf Download
- Products need to be added to orders one by one and not in mass (NOT like it currently does)
- Product s need to be deleted from orders one by one and not in mass (NOT like it currently does)
- Bonus: User can assign order to a different user.

## Installation 

1. Run ``composer install`` 
2.  
    Copy the .env.example file into .env
     
    ``cp .env.example .env``
3. Change Database Details
4. Run 

    ```php artisan migrate --seed```
5. Order System located at 

    `yourdomain.test/orders`
    
    
    
    
## EXTRA SETUP
1. npm run dev

2. php artisan migrate

3. DON'T FORGET TO REGISTER NEW USER