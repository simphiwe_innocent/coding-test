<?php

namespace App\Http\Controllers;

use App\Order;
use App\OrderItem;
use App\Product;
use App\User;
use App\Variant;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use PDF;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('orders.index', ['orders' => Order::all()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('orders_create', ['users' => User::all(), 'products' => Product::with('variants')->get()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


        //get users order
        $order = Order::where('user_id', Auth::User()->id)->first();

        if (!$order) {
            $order = new Order();
        }

        $order->user_id = Auth::user()->id;

        if ($order->save()) {
            $order_item = new OrderItem();
            $order_item->qty = $request->input('qty');
            $order_item->order_id = $order->id;
            $order_item->product_id = $request->input('productId');
            $order_item->variant_id = $request->input('variant');
            $order_item->save();
        }

        return redirect()->route('orders.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function show(Order $order)
    {
        return view('orders_view', [
            'order' => $order->load('orderItems')
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function edit(Order $order)
    {
        return view('orders.edit', [
            'order' => $order->load('orderItems'), 'users' => User::all(),
            'products' => Product::with('variants')->get()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Order $order)
    {
        $data = $request->validate([
            'user_id' => 'required',
        ]);

        $order->update(['user_id' => $data['user_id']]);

        return redirect()->route('orders.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function destroy(Order $order)
    {
        $order->delete();

        return redirect()->route('orders.index');
    }

    private function saveOrderDetails(Order $order, $data)
    {
        foreach ($data['qty'] as $key => $val)
        {
            foreach ($val as $k => $v)
            {
                if($v > 0)
                {
                    $order_item = new OrderItem();
                    $order_item->qty = $v;
                    $order_item->order_id = $order->id;
                    $order_item->product_id = $key;
                    $order_item->variant_id = $k;
                    $order_item->save();
                }
            }
        }
    }

    /**
     * @param Product $product
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function orderProduct(Product $product)
    {
        return view('orders.order-product', [
            'product' => $product,
            'variant' => Variant::all()
            ]);
    }

    /**
     * @param Order $order
     * @return mixed
     */
    public function exportPDF(Order $order)
    {
        $order = $order->load('orderItems');
        $pdf = PDF::loadView('order_pdf_view', [
            'order' => $order->load('orderItems')
        ]);
        // download PDF file with download method

        return $pdf->download('pdf_file.pdf');
    }
}
